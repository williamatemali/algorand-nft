const algosdk = require('algosdk');

/*** Functions from previous example ***/
function generateAlgorandKeyPair() {
    const account = algosdk.generateAccount();
    const passphrase = algosdk.secretKeyToMnemonic(account.sk);
    console.log("My address: " + account.addr);
    console.log("My passphrase: " + passphrase);

    var recoveredAccount = algosdk.mnemonicToSecretKey(passphrase);
    var isValid = algosdk.isValidAddress(recoveredAccount.addr);
    console.log("Is this a valid address: " + isValid);
    console.log("Account created. Save off Mnemonic and address");
    console.log("");
    console.log("Add funds to the account using the TestNet Dispenser at https://bank.testnet.algorand.network/ ");
    console.log("");
    console.log("Press any key when the account is funded");
    return account;
}

// Function used to wait for a tx confirmation
const waitForConfirmation = async function (algodclient, txId) {
    let response = await algodclient.status().do();
    let lastround = response["last-round"];
    while (true) {
        const pendingInfo = await algodclient.pendingTransactionInformation(txId).do();
        if (pendingInfo["confirmed-round"] !== null && pendingInfo["confirmed-round"] > 0) {
            //Got the completed Transaction
            console.log("Transaction " + txId + " confirmed in round " + pendingInfo["confirmed-round"]);
            break;
        }
        lastround++;
        await algodclient.statusAfterBlock(lastround).do();
    }
};


// Function used to print created asset for account and assetid
const printCreatedAsset = async function (algodclient, account, assetid) {
    // note: if you have an indexer instance available it is easier to just use this
    //     let accountInfo = await indexerClient.searchAccounts()
    //    .assetID(assetIndex).do();
    // and in the loop below use this to extract the asset for a particular account
    // accountInfo['accounts'][idx][account]);
    let accountInfo = await algodclient.accountInformation(account).do();
    for (idx = 0; idx < accountInfo['created-assets'].length; idx++) {
        let scrutinizedAsset = accountInfo['created-assets'][idx];
        if (scrutinizedAsset['index'] == assetid) {
            console.log("AssetID = " + scrutinizedAsset['index']);
            let myparms = JSON.stringify(scrutinizedAsset['params'], undefined, 2);
            console.log("parms = " + myparms);
            break;
        }
    }
};
// Function used to print asset holding for account and assetid
const printAssetHolding = async function (algodclient, account, assetid) {
    // note: if you have an indexer instance available it is easier to just use this
    //     let accountInfo = await indexerClient.searchAccounts()
    //    .assetID(assetIndex).do();
    // and in the loop below use this to extract the asset for a particular account
    // accountInfo['accounts'][idx][account]);
    let accountInfo = await algodclient.accountInformation(account).do();
    for (idx = 0; idx < accountInfo['assets'].length; idx++) {
        let scrutinizedAsset = accountInfo['assets'][idx];
        if (scrutinizedAsset['asset-id'] == assetid) {
            let myassetholding = JSON.stringify(scrutinizedAsset, undefined, 2);
            console.log("assetholdinginfo = " + myassetholding);
            break;
        }
    }
};

const keypress = async () => {
    process.stdin.setRawMode(true)
    return new Promise(resolve => process.stdin.once('data', () => {
        process.stdin.setRawMode(false)
        resolve()
    })) 
}


/*** Main code ***/


// sandbox
const token = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
const server = "http://localhost";
const port = 4001;
// Instantiate the algod wrapper
let algodclient = new algosdk.Algodv2(token, server, port);

(async () => {

    console.log("=====Create Account=====");
    const account = await generateAlgorandKeyPair();

    await keypress();

    let params = await algodclient.getTransactionParams().do();
    params.fee = 1000;
    params.flatFee = true;
    console.log(params);
    let note = undefined; 
    let addr = account.addr;   
    let defaultFrozen = false;
    // For pure NFT, this field should be zero
    // For fractional NFT, Decimals MUST be equal to the logarithm in base 10 of total number of units: 10
    let decimals = 0;
    // For pure NFT, this field should be 1
    // For fractional NFT, Total MUST be a power of 10 larger than 1: 10, 100, 1000, ...(total) * .1(each) = 1.0 
    let totalIssuance = 1;
    // Used to display asset units to user    
    let unitName = "DEMOART";
    // Friendly name of the asset    
    let assetName = "Demo Artwork@arc3";
    // Optional string pointing to a URL relating to the asset
    let assetURL = "http://localhost/aeestMetadata/demo.json";
    // Optional hash commitment of some sort relating to the asset. 32 character length.
    let assetMetadataHash = "16efaa3924a6fd9d3a4824799a4ac65d";
    let manager = undefined;
    let reserve = undefined;
    let freeze = undefined;
    let clawback = undefined;

    // signing and sending "txn" allows "addr" to create an asset
    let txn = algosdk.makeAssetCreateTxnWithSuggestedParams(addr, note,
         totalIssuance, decimals, defaultFrozen, manager, reserve, freeze,
        clawback, unitName, assetName, assetURL, assetMetadataHash, params);


    console.log("=====Sign the transaction=====");
    
    let rawSignedTxn = txn.signTxn(account.sk)


    console.log("=====Send the transaction=====");
    let tx = (await algodclient.sendRawTransaction(rawSignedTxn).do());
    console.log("Transaction : " + tx.txId);
    let assetID = null;
    // wait for transaction to be confirmed
    await waitForConfirmation(algodclient, tx.txId);
    // Get the new asset's information from the creator account
    let ptx = await algodclient.pendingTransactionInformation(tx.txId).do();
    assetID = ptx["asset-index"];
   // console.log("AssetID = " + assetID);
    
    console.log("=====Print Created Asset=====");
    await printCreatedAsset(algodclient, account.addr, assetID);
    console.log("=====Print Account Holdings=====");
    await printAssetHolding(algodclient, account.addr, assetID);
    console.log("=====End=====");


    process.exit();

})().catch(e => {
    console.log(e);
    console.trace();
});