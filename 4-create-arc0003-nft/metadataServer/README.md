# A Simple Express node.js Server that serve the resource

## General information

Install the dependencies
>`npm install`

Run the Server
>`docker-compose up -d`

Stop the Server
>`docker-compose down`

## Change the port
The default port using is 80. If you want to change the port, update the following files:

1. test.env (Or you can build you own env)
2. Update the EXPOSE in Dockerfile
3. Update the ports in docker-compose.yml 