
const express = require("express");
const cors = require('cors');
const app = express();
// Configure CORS
app.use(cors());
var port = process.env.EXPOSE_PORT

if(port == 80){
    //docker
    app.use(express.static('public'));
}

app.listen(port, () => console.log('Node server listening on port '+port+'!'));
