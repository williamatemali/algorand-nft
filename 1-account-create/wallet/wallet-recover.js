const algosdk = require('algosdk');

//No need to change if you are using the sandbox 
const kmdtoken = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
const kmdserver = "http://localhost";
const kmdport = "4002";

const kmdclient = new algosdk.Kmd(kmdtoken, kmdserver, kmdport);

let walletid = null;
let wallethandle = null;

//Wallet parameter
const walletName = "Wallet";
const walletPassword = "testpassword";
const walletMDKMnemonic = "equip mirror ability abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon abandon cool";
const walletDriverName = "sqlite";

(async () => {
    console.log("Recovery Wallet From Mnemonic");

    let mdk =  (await algosdk.mnemonicToMasterDerivationKey(walletMDKMnemonic));

    let walletid = (await kmdclient.createWallet(walletName, walletPassword, mdk,walletDriverName)).wallet.id;
    console.log("Created wallet:", walletid);

    let wallethandle = (await kmdclient.initWalletHandle(walletid, walletPassword)).wallet_handle_token;
    console.log("Got wallet handle:", wallethandle);

    let address1 = (await kmdclient.generateKey(wallethandle)).address;
    console.log("Created new account:", address1);

    let address2 = (await kmdclient.generateKey(wallethandle)).address;
    console.log("Created new account:", address2);

    let address3 = (await kmdclient.generateKey(wallethandle)).address;
    console.log("Created new account:", address3);
})().catch(e => {
    console.log(e);
});