const algosdk = require('algosdk');

//No need to change if you are using the sandbox 
const kmdtoken = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
const kmdserver = "http://localhost";
const kmdport = "4002";

const kmdclient = new algosdk.Kmd(kmdtoken, kmdserver, kmdport);

let wallethandle = null;

//Wallet parameter
const walletPassword = "testpassword";
//wallet id from wallet-create
const walletid = "192506df470650e12bd5c88d9be19bb6";
//The address to be deleted
const deleteAddress = "OJKPZ7HP6XQWHKM75HUOGWSE6FI23HNOR6WM3CPXPLA5IHTFQKXWVBGMPQ";

(async () => {
    let wallethandle = (await kmdclient.initWalletHandle(walletid, walletPassword)).wallet_handle_token;
    console.log("Got wallet handle:", wallethandle);

    let address1 = (await kmdclient.deleteKey(wallethandle, walletPassword, deleteAddress));
})().catch(e => {
    console.log(e);
});