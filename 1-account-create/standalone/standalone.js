/*
 * Date: 2020-06-24
 *
 * Generate a standalone account using the algosdk.
 */

const algosdk = require('algosdk');

function generateAlgorandKeyPair() {
    const account = algosdk.generateAccount();
    const passphrase = algosdk.secretKeyToMnemonic(account.sk);
    console.log("My address: " + account.addr);
    console.log("My passphrase: " + passphrase);
}

generateAlgorandKeyPair();
